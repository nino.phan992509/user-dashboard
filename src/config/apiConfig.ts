import axios from "axios";

const baseURL = 'https://650112da736d26322f5b4d71.mockapi.io'

const apiConfig = () => {
  return axios.create({
      baseURL,
      responseType: 'json',
  })
  
}

export default apiConfig;