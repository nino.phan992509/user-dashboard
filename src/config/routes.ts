import IRoute from '@interfaces/IRoute'
import UserDashboard from '../views/user-dashboard'

const routes: IRoute[] = [
    {
        path: '/',
        name: 'User Dashboard',
        component: UserDashboard,
        exact: true,
    },
]

export default routes
