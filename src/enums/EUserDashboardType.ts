export type UserType = {
    id?: string
    username: string
    roleId: string
}

export type RoleType = {
    id: string
    name: string
}
