import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import Application from './Application'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

ReactDOM.render(
    <React.StrictMode>
        <Application />
        <ToastContainer/>
    </React.StrictMode>,
    document.getElementById('root')
)
