import UserDashboardComponent from '@components/user-dashboard'

const UserDashboardView = () => {
    return <UserDashboardComponent />
}

export default UserDashboardView
