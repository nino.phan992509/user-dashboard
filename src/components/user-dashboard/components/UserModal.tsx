import { useFormik } from 'formik'
import * as yup from 'yup'

import Button from '@mui/material/Button'
import {
    Autocomplete,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    FormHelperText,
    InputLabel,
    MenuItem,
    Select,
    Stack,
    TextField,
} from '@mui/material'

import { RoleType, UserType } from '@enums/EUserDashboardType'
import { useEffect } from 'react'

const validationSchema = yup.object({
    username: yup.string().required('Username is required'),
    roleId: yup.string().required('Role is required'),
})

interface UserModalProps {
    isOpen: boolean
    roles: RoleType[]
    currentUser?: UserType
    onClose: VoidFunction
    onSubmit: (data: UserType) => void
}

export default function UserModal(props: UserModalProps) {
    const { isOpen, roles, currentUser, onClose, onSubmit } = props

    const formik = useFormik({
        initialValues: {
            username: '',
            roleId: '',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => onSubmit(values),
    })

    useEffect(() => {
        currentUser && formik.setValues(currentUser)
        return () => {}
    }, [currentUser])

    return (
        <Dialog open={isOpen} onClose={onClose}>
            <DialogTitle>
                {currentUser ? 'Update User' : 'Add User'}
            </DialogTitle>
            <DialogContent>
                <form onSubmit={formik.handleSubmit}>
                    <Stack
                        direction={'column'}
                        spacing={2}
                        sx={{ minWidth: 320 }}
                        py={1}
                    >
                        <TextField
                            fullWidth
                            id="form-username"
                            name="username"
                            label="Username"
                            value={formik.values.username}
                            onChange={formik.handleChange}
                            error={
                                formik.touched.username &&
                                Boolean(formik.errors.username)
                            }
                            helperText={
                                formik.touched.username &&
                                formik.errors.username
                            }
                        />

                        <FormControl>
                            <InputLabel id="form-select-role-label">
                                Role
                            </InputLabel>
                            <Select
                                labelId="form-select-role-label"
                                id="form-select-role"
                                value={formik.values.roleId}
                                label="Role"
                                name="RoleId"
                                onChange={(e) =>
                                    formik.setFieldValue(
                                        'roleId',
                                        e.target.value
                                    )
                                }
                                // onBlur={formik.handleBlur}
                            >
                                {roles.map((role) => (
                                    <MenuItem value={role.id} key={role.id}>
                                        {role.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>

                        <FormHelperText
                            error={
                                formik.touched.roleId &&
                                Boolean(formik.errors.roleId)
                            }
                            title={
                                (formik.touched.roleId &&
                                    formik.errors.roleId) ||
                                ''
                            }
                        />
                        <Button
                            color="primary"
                            variant="contained"
                            fullWidth
                            type="submit"
                        >
                            {currentUser ? 'Update' : 'Add'}
                        </Button>
                    </Stack>
                </form>
            </DialogContent>
        </Dialog>
    )
}
