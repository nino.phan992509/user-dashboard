import { useEffect, useState } from 'react'
import { toast } from 'react-toastify'

import { styled } from '@mui/material/styles'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell, { tableCellClasses } from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Paper from '@mui/material/Paper'
import {
    Button,
    Card,
    FormControl,
    IconButton,
    InputLabel,
    MenuItem,
    Select,
    Stack,
    TableFooter,
    TablePagination,
} from '@mui/material'
import { Add, Delete, Edit } from '@mui/icons-material'

import TablePaginationActions from './components/TablePaginationActions'
import UserModal from './components/UserModal'

import { RoleType, UserType } from '@enums/EUserDashboardType'
import apiConfig from '@config/apiConfig'

const columns = [
    {
        id: 'id',
        name: 'ID',
    },
    {
        id: 'username',
        name: 'Username',
    },
    {
        id: 'roleId',
        name: 'Role',
    },
]

export default function UserDashboard() {
    const [rows, setRows] = useState<UserType[]>([])
    const [roles, setRoles] = useState<RoleType[]>([])
    const [page, setPage] = useState<number>(0)
    const [rowsPerPage, setRowsPerPage] = useState<number>(5)
    const [isOpen, setIsOpen] = useState<boolean>(false)
    const [roleId, setRoleId] = useState<string>('')
    const [currentUser, setCurrentUser] = useState<UserType>()

    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0

    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number
    ) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10))
        setPage(0)
    }

    const getAllRoles = async () => {
        try {
            const result = await apiConfig().get('/roles')
            setRoles(result.data)
        } catch (error) {
            console.log(error)
        }
    }

    const fetchData = async () => {
        try {
            const result = await apiConfig().get(`/roles/${roleId}/users`)
            setRows(result.data)
        } catch (error) {
            console.log(error)
        }
    }

    const addUser = async (data: UserType) => {
        try {
            await apiConfig().post(`/roles/${data.roleId}/users`, data)
            toggleModal()
            toast.success('Added new user successfully!')
            fetchData()
        } catch (error) {
            console.log(error)
            toast.error('Something went wrong! Please try again!')
        }
    }

    const updateUser = async (data: UserType) => {
        try {
            await apiConfig().put(
                `/roles/${data.roleId}/users/${data.id}`,
                data
            )
            toggleModal()
            toast.success('Updated user successfully!')
            fetchData()
            setCurrentUser(undefined)
        } catch (error) {
            console.log(error)
            toast.error('Something went wrong! Please try again!')
        }
    }

    const deleteUser = async (data: UserType) => {
        try {
            await apiConfig().delete(`/roles/${data.roleId}/users/${data.id}`)
            toast.success('Deleted user successfully!')
            fetchData()
        } catch (error) {
            console.log(error)
            toast.error('Something went wrong! Please try again!')
        }
    }

    const handleSubmitForm = (data: UserType) => {
        if (data.id) {
            updateUser(data)
        } else {
            addUser(data)
        }
    }

    const toggleModal = (user?: UserType) => {
        user ? setCurrentUser(user) : currentUser && setCurrentUser(undefined)
        setIsOpen(!isOpen)
    }

    const handleClickEdit = (user: UserType) => {
        toggleModal(user)
    }

    useEffect(() => {
        getAllRoles()
        return () => {}
    }, [])

    useEffect(() => {
        roles.length > 0 && setRoleId(roles?.[0]?.id)
    }, [roles])

    useEffect(() => {
        roleId && fetchData()
    }, [roleId])

    return (
        <Card sx={{ padding: 2 }}>
            <UserModal
                isOpen={isOpen}
                onClose={toggleModal}
                roles={roles}
                onSubmit={handleSubmitForm}
                currentUser={currentUser}
            />
            <Stack
                width={'100%'}
                my={3}
                spacing={2}
                direction={'row'}
                justifyContent={'flex-end'}
            >
                <FormControl>
                    <InputLabel id="select-role-label">Role</InputLabel>
                    <Select
                        labelId="select-role-label"
                        id="select-role"
                        value={roleId}
                        label="Role"
                        name="Role"
                        onChange={(e) => setRoleId(e.target.value)}
                        sx={{ width: 300 }}
                    >
                        {roles.map((role) => (
                            <MenuItem value={role.id} key={role.id}>
                                {role.name}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <Button
                    startIcon={<Add />}
                    variant="contained"
                    onClick={() => toggleModal()}
                >
                    Add New User
                </Button>
            </Stack>

            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <StyledTableCell key={column.id} align="center">
                                    {column.name}
                                </StyledTableCell>
                            ))}
                            <StyledTableCell align="center">
                                Action
                            </StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                            ? rows.slice(
                                  page * rowsPerPage,
                                  page * rowsPerPage + rowsPerPage
                              )
                            : rows
                        ).map((row) => (
                            <StyledTableRow key={row.id}>
                                <StyledTableCell
                                    component="th"
                                    scope="row"
                                    align="center"
                                >
                                    {row.id}
                                </StyledTableCell>
                                <StyledTableCell align="center">
                                    {row.username}
                                </StyledTableCell>
                                <StyledTableCell align="center">
                                    {roles.find(
                                        (role) => role.id === row.roleId
                                    )?.name || row.roleId}
                                </StyledTableCell>
                                <StyledTableCell align="center">
                                    <Stack
                                        direction="row"
                                        spacing={1}
                                        justifyContent={'center'}
                                        alignItems="center"
                                    >
                                        <IconButton
                                            onClick={() => handleClickEdit(row)}
                                        >
                                            <Edit />
                                        </IconButton>
                                        <IconButton
                                            onClick={() => deleteUser(row)}
                                        >
                                            <Delete />
                                        </IconButton>
                                    </Stack>
                                </StyledTableCell>
                            </StyledTableRow>
                        ))}
                        {emptyRows > 0 && (
                            <StyledTableRow style={{ height: 53 * emptyRows }}>
                                <TableCell colSpan={6} />
                            </StyledTableRow>
                        )}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                rowsPerPageOptions={[
                                    5,
                                    10,
                                    25,
                                    { label: 'All', value: -1 },
                                ]}
                                colSpan={3}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: {
                                        'aria-label': 'rows per page',
                                    },
                                    native: true,
                                }}
                                onPageChange={handleChangePage}
                                onRowsPerPageChange={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </Card>
    )
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}))

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}))
