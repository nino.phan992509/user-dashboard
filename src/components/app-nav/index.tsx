import React, { FC } from 'react'
import GithubIcon from '../../assets/icons/github.png';
import { langs } from '@locales/langs';
import { useTranslation } from 'react-i18next';
import styles from './styles.module.scss'

const AppNav: FC = () => {
    const {i18n} = useTranslation();
    return (
        <div className={styles.appNavContainer}>
        </div>
    )
}
export default AppNav
